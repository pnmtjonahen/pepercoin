#!/bin/sh
cargo clean
LLVM_PROFILE_FILE="./target/coverage/gcd-%p-%m.profraw" RUSTFLAGS="-Z instrument-coverage" cargo test
cargo profdata -- merge -sparse ./target/coverage/gcd-*.profraw -o ./target/coverage/gcd.profdata
cargo cov -- show \
    $( \
      for file in \
        $( \
          RUSTFLAGS="-Z instrument-coverage" \
            cargo test --tests --no-run --message-format=json \
              | jq -r "select(.profile.test == true) | .filenames[]" \
              | grep -v dSYM - \
        ); \
      do \
        printf "%s %s " -object $file; \
      done \
    ) \
    -object ./target/debug/gcd-add \
    -object ./target/debug/gcd-alias \
    -object ./target/debug/gcd-delete \
    -object ./target/debug/gcd-init \
    -object ./target/debug/gcd-mv \
    -object ./target/debug/gcd-purge \
    -object ./target/debug/gcd-select \
    -object ./target/debug/gcd-update \
  --instr-profile=./target/coverage/gcd.profdata --format=html --output-dir=./target/coverage/report --ignore-filename-regex='/.cargo/registry|/rustc'
