#!/bin/bash

# start script for GCD command. This allows the cd to the target folder/project to work.
# this script also needs to be source(d), make an bash alias gcd='. gcd' to allow for this

SCRIPT_FILE="${HOME}/.gcd/gcd-cd.sh"

gcd-select $@

if [[ -f "${SCRIPT_FILE}" ]]; then
  source ${SCRIPT_FILE}
  rm ${SCRIPT_FILE}
fi
