use anyhow::Result;
use glob::glob;
use indicatif::ProgressBar;

use crate::lib::to_string;

pub fn find_projects(root_folder: &str) -> Result<Vec<String>> {
    let spinner = ProgressBar::new_spinner();
    spinner.set_message(format!(
        "Searching for git project with base folder {}",
        root_folder
    ));

    let mut projects: Vec<String> = vec![];
    for mut path in glob(&format!("{}/**/.git", root_folder))
        ?
        .filter_map(Result::ok)
        .filter(|p| p.is_dir())
    {
        spinner.tick();
        path.pop();
        projects.push(to_string(path));
    }
    spinner.finish_with_message("done.");
    Ok(projects)
}
#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_find_projects() -> Result<()> {
        let projects = find_projects(".")?;
        assert_eq!(projects.len() >= 1, true);
        Ok(())
    }
}