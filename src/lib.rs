
extern crate clap;
extern crate console;
extern crate regex;
extern crate rusqlite;
extern crate glob;
extern crate indicatif;
extern crate ctrlc;

pub mod error;
pub mod inputhandler;
pub mod outputhandler;
pub mod scriptfile;
pub mod db;
pub mod projectsfinder;
pub mod config;
pub mod constants;

pub mod lib {
    use std::path::PathBuf;
    use anyhow::Result;

    pub fn current_dir() -> Result<String> {
        Ok(to_string(std::env::current_dir()?))
    }

    pub fn to_string(path: PathBuf) -> String {
        path.to_string_lossy().to_string()
    }
}

#[cfg(test)]
mod test {
    use std::path::PathBuf;
    use crate::lib::to_string;


    #[test]
    fn test_to_string() {
        let path = PathBuf::from("/home/test/");

        assert_eq!(to_string(path), "/home/test/");
    }

}