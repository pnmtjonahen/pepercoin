use std::{error::Error, fmt};

#[derive(Debug)]
pub struct GcdError {
    message: String,
}

impl Error for GcdError {}

impl GcdError {
    pub fn new(message: String) -> GcdError {
        GcdError { message }
    }
}

impl fmt::Display for GcdError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "gcd-error:{}", &self.message)
    }
}


#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn display_error() {
        let error = GcdError::new("test error message".to_owned());

        assert_eq!("gcd-error:test error message", format!("{}", error));
    }

    #[test]
    fn print_for_cov() {
        let error = GcdError::new("test error message".to_owned());
        println!("{}", error);
    }
}