use std::io;

pub struct ScriptFile{
    script_file_name: String,
}

impl ScriptFile {
    pub fn new(script_file: String) -> Self {
        ScriptFile{
            script_file_name : script_file,
        }
    }

    pub fn write_cd_only(&self, project: &String) -> io::Result<()>{
        script::write_cd_only(&self.script_file_name, project)
    }
    pub fn write_cd_and_exec(&self, project: &String, command: &Vec<String>) -> io::Result<()>{
        script::write_cd_and_exec(&self.script_file_name, project, command)
    }
}

#[cfg(unix)]
mod script {
    use std::io;
    use std::fs;

    pub fn write_cd_only(script_file_name: &String, project: &String) -> io::Result<()>{
        let data = format!("#!/bin/bash\ncd {}\n", project);
        fs::write(script_file_name, data)
    }
    pub fn write_cd_and_exec(script_file_name: &String, project: &String, command: &Vec<String>) -> io::Result<()>{
        let data = format!("#!/bin/bash\ncd {}\n{}\n", project, command.join(" "));
        fs::write(script_file_name, data)
    }
}

#[cfg(windows)]
mod script {
    use std::io;

    use std::fs;

    pub fn write_cd_only(script_file_name: String, project: String) -> io::Result<()>{
        let data = format!("cd /d {}\n", project);
        fs::write(script_file_name, data)
    }
    pub fn write_cd_and_exec(script_file_name: String, project: String, command: Vec<String>) -> io::Result<()>{
        let data = format!("cd /d {}\n{}\n", project, command.join(" "));
        fs::write(script_file_name, data)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[cfg(unix)]
    #[test]
    fn write_cd_only() {
        let script_file = ScriptFile::new("target/tmp/test_cd_only".to_owned());

        assert!(script_file.write_cd_only(&"sample".to_string()).is_ok());
        assert_eq!(std::fs::read_to_string("target/tmp/test_cd_only").unwrap(), "#/bin/bash\ncd sample\n");

    }

    #[cfg(unix)]
    #[test]
    fn write_cd_and_exec() {
        let script_file = ScriptFile::new("target/tmp/test_cd_and_exec".to_owned());

        assert!(script_file.write_cd_and_exec(&"sample".to_string(), &vec!["ls".to_owned()]).is_ok());
        assert_eq!(std::fs::read_to_string("target/tmp/test_cd_and_exec").unwrap(), "#/bin/bash\ncd sample\nls\n");


    }

}