use console::Term;
use std::io;

pub struct OutputHandler {
    projects_dir: String,
    cur: usize,
    term: Term,
    projects: Vec<String>,
}

impl OutputHandler {
    pub fn new(projects_dir: String) -> Self {
        OutputHandler {
            projects_dir,
            cur: 0,
            term: Term::stdout(),
            projects: vec![],
        }
    }

    pub fn up(&mut self) -> io::Result<usize> {
        if self.cur == 0 {
            self.cur = 0;
        } else {
            self.cur -= 1;
        }
        self.show()
    }

    pub fn down(&mut self) -> io::Result<usize> {
        if self.cur < self.projects.len() - 1 {
            self.cur += 1;
        }
        self.show()
    }

    fn show(&mut self) -> io::Result<usize> {
        self.clear_results()?;
        let mut index = 0;
        self.term.move_cursor_down(1)?;
        for project in self
            .projects
            .iter()
            .map(|p| p.replace(&self.projects_dir, ""))
        {
            if index == self.cur {
                term_extra::set_foreground_color(2)?;
                self.term.write_line(&format!(" > {}", project))?;
                term_extra::turn_off_all_attributes()?;
            } else {
                self.term.write_line(&format!("   {}", project))?;
            }
            index += 1;
            if index > 20 {
                break;
            }
        }
        self.term.move_cursor_up(index + 1)?;
        Ok(index)
    }

    pub fn show_results(&mut self, projects: Vec<String>) -> io::Result<usize> {
        self.projects = projects.iter().map(|p| p.to_owned()).collect();
        self.cur = 0;
        self.show()
    }

    pub fn get_selected(&mut self) -> Option<String> {
        let _ = self.clear_results();
        if self.projects.len() > self.cur {
            return Some(self.projects[self.cur].to_owned());
        }
        None
    }

    pub fn clear_results(&mut self) -> io::Result<()> {
        self.term.clear_to_end_of_screen()?;
        Ok(())
    }
}

#[cfg(unix)]
mod term_extra {
    use std::io;
    use std::io::Write;

    pub fn set_foreground_color(code: usize) -> io::Result<()> {
        io::stdout().write_all(&format!("\x1b[{}m", code).as_bytes())?;
        io::stdout().flush()?;
        Ok(())
    }

    pub fn turn_off_all_attributes() -> io::Result<()> {
        io::stdout().write_all(&format!("\x1b[0m").as_bytes())?;
        io::stdout().flush()?;
        Ok(())
    }
}

#[cfg(windows)]
mod term_extra {
    use std::io;

    // not implemented yet, needs to be implemented in console first
    pub fn set_foreground_color(_code: usize) -> io::Result<()> {
        Ok(())
    }

    pub fn turn_off_all_attributes() -> io::Result<()> {
        Ok(())
    }
}
