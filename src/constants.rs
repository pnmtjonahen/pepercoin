
pub const DATABASE_FILE: &'static str = "db_file";
pub const DATABASE_FILE_HELP: &'static str = "SQLITE Database file to store project and usage data.";
pub const DATABASE_FILE_VALUE_NAME: &'static str = "DATABASE_FILE";

pub const PROJECTS_DIR: &'static str = "projects_dir";
pub const PROJECTS_DIR_HELP: &'static str = "Projects base dir, projects are relative to this path.";
pub const PROJECTS_DIR_VALUE_NAME: &'static str = "PROJECTS_DIR";

pub const SCRIPT_FILE: &'static str = "script_file";
pub const SCRIPT_FILE_HELP: &'static str = "This is the script file that wil contain the selected project.";
pub const SCRIPT_FILE_VALUE_NAME: &'static str = "SCRIPT_FILE";

pub const FIND: &'static str = "find";
pub const FIND_HELP: &'static str = "project to find";

pub const EXEC_COMMAND: &'static str = "exec";
pub const EXEC_COMMAND_HELP: &'static str = "Execute COMMAND in selected folder.";
pub const EXEC_COMMAND_VALUE_NAME: &'static str = "COMMAND";

pub const FROM_LOCATION: &'static str = "from_location";
pub const TO_LOCATION: &'static str = "to_location";
