use anyhow::Result;
use clap::{crate_authors, crate_version, Command, Arg, ArgAction};

use gcd_cli::config::Config;
use gcd_cli::constants::*;
use gcd_cli::db::Database;
use gcd_cli::lib::current_dir;

const ALIAS: &'static str = "alias";

fn main() -> Result<()> {
    let config = Config::new()?;
    let default_database_file = config.database_file();

    let matches = Command::new("gcd-alias")
        .version(&crate_version!()[..])
        .author(crate_authors!())
        .about("Add/Remove an alias or list aliasses for the current folder.")
        .arg(
            Arg::new("list")
                .short('l')
                .long("list")
                .help("list all aliases")
                .required(false)
                .num_args(0)
                .conflicts_with("remove")
                .action(ArgAction::SetTrue),
        )
        .arg(
            Arg::new("remove")
                .short('r')
                .long("remove")
                .help("Removes alias for current folder")
                .required(false)
                .num_args(0)
                .conflicts_with("list")
                .action(ArgAction::SetTrue),
        )
        .arg(
            Arg::new(DATABASE_FILE)
                .short('d')
                .long(DATABASE_FILE)
                .env(DATABASE_FILE)
                .value_name(DATABASE_FILE_VALUE_NAME)
                .default_value(&default_database_file)
                .help(DATABASE_FILE_HELP)
                .required(false)
                .num_args(1),
        )
        .arg(
            Arg::new(ALIAS)
                .help("alias for current project")
                .required_unless_present_any(&["list", "remove"]),
        )
        .get_matches();

    let database_file = matches
        .get_one(DATABASE_FILE)
        .unwrap_or(&default_database_file);
    let database = Database::new(database_file)?;

    if matches.get_flag("list") {
        list(database.all_aliased()?);
    }
    if matches.get_flag("remove") {
        if let Some(alias) = matches.get_one::<String>(ALIAS) {
            database.remove_alias_by_alias(alias)?;
        } else {
            database.remove_alias(current_dir()?.as_str())?;
        }
    } else {
        if let Some(alias) = matches.get_one::<String>(ALIAS) {
            database.alias(current_dir()?.as_str(), alias)?;
        }
    }
    Ok(())
}

fn list(projects: Vec<(String, String)>) {
    for project in projects {
        println!("{:>15} - {}", project.0, project.1);
    }
}
