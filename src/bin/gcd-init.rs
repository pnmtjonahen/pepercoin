use anyhow::Result;
use clap::{Command, Arg, crate_version, crate_authors};
use gcd_cli::db::Database;
use gcd_cli::constants::*;
use gcd_cli::projectsfinder::find_projects;
use gcd_cli::config::Config;

fn main() -> Result<()> {
    let config = Config::new()?;
    let default_database_file = config.database_file();
    let default_projects_dir = config.projects_dir();

    let matches = Command::new("gcd-init")
        .version(&crate_version!()[..])
        .author(crate_authors!())
        .about("(Re)-initialize the database. This wil clear all current projects and aliases.")
        .arg(
            Arg::new(PROJECTS_DIR)
                .short('p')
                .long(PROJECTS_DIR)
                .env(PROJECTS_DIR)
                .value_name(PROJECTS_DIR_VALUE_NAME)
                .default_value(&default_projects_dir)
                .help(PROJECTS_DIR_HELP)
                .required(false)
                .num_args(1),
        )
        .arg(
            Arg::new(DATABASE_FILE)
                .short('d')
                .long(DATABASE_FILE)
                .env(DATABASE_FILE)
                .value_name(DATABASE_FILE_VALUE_NAME)
                .default_value(&default_database_file)
                .help(DATABASE_FILE_HELP)
                .required(false)
                .num_args(1),
        )
        .get_matches();

    let database_file = matches
        .get_one(DATABASE_FILE)
        .unwrap_or(&default_database_file);
    let projects_dir = matches
        .get_one(PROJECTS_DIR)
        .unwrap_or(&default_projects_dir);

    let database = Database::new(database_file)?;
    let lines = find_projects(projects_dir)?;
    database.add(&lines)?;
    Ok(())
}
