use anyhow::Result;
use clap::{Command, Arg, ArgMatches, crate_authors, crate_version};

use gcd_cli::config::Config;
use gcd_cli::constants::*;
use gcd_cli::db::Database;

fn main() -> Result<()> {
    let config = Config::new()?;
    let default_database_file = config.database_file();

    let matches = Command::new("gcd-mv")
        .version(&crate_version!()[..])
        .author(crate_authors!())
        .about("Move a project to a new location.")
        .arg(
            Arg::new(DATABASE_FILE)
                .short('d')
                .long(DATABASE_FILE)
                .env(DATABASE_FILE)
                .value_name(DATABASE_FILE_VALUE_NAME)
                .default_value(&default_database_file)
                .help(DATABASE_FILE_HELP)
                .required(false)
                .num_args(1),
        )
        .arg(
            Arg::new(FROM_LOCATION)
                .help("Current project location")
                .required(true),
        )
        .arg(
            Arg::new(TO_LOCATION)
                .help("New project location")
                .required(true),
        )
        .get_matches();

    let database_file = matches
        .get_one(DATABASE_FILE)
        .unwrap_or(&default_database_file);

    let database = Database::new(database_file)?;

    let (cur_loc, new_loc) = safe_get_required(matches);
    database.move_project(cur_loc.as_str(), new_loc.as_str())?;
    Ok(())
}

fn safe_get_required(matches: ArgMatches) -> (String, String) {
    (matches.get_one::<String>(FROM_LOCATION).unwrap().to_owned(), matches.get_one::<String>(TO_LOCATION).unwrap().to_owned())
}
