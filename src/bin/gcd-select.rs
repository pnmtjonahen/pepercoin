use anyhow::Result;
use console::Key::*;
use std::sync::{
    atomic::{AtomicBool, Ordering},
    Arc,
};

use clap::{crate_authors, crate_version, Arg, ArgAction, Command};

use gcd_cli::config::Config;
use gcd_cli::constants::*;
use gcd_cli::db::Database;
use gcd_cli::inputhandler::InputHandler;
use gcd_cli::outputhandler::OutputHandler;
use gcd_cli::scriptfile::ScriptFile;

fn main() -> Result<()> {
    let running = Arc::new(AtomicBool::new(true));
    let r = running.clone();

    ctrlc::set_handler(move || {
        r.store(false, Ordering::SeqCst);
    })?;

    let config = Config::new()?;
    let default_projects_dir = config.projects_dir();
    let default_script_file = config.script_file();
    let default_database_file = config.database_file();

    let matches = Command::new("gcd-select")
        .version(&crate_version!()[..])
        .author(crate_authors!())
        .about("Select a project.")
        .arg(
            Arg::new(PROJECTS_DIR)
                .short('p')
                .long(PROJECTS_DIR)
                .env(PROJECTS_DIR)
                .value_name(PROJECTS_DIR_VALUE_NAME)
                .default_value(&default_projects_dir)
                .help(PROJECTS_DIR_HELP)
                .required(false)
                .num_args(1),
        )
        .arg(
            Arg::new(SCRIPT_FILE)
                .short('s')
                .long(SCRIPT_FILE)
                .env(SCRIPT_FILE)
                .value_name(SCRIPT_FILE_VALUE_NAME)
                .default_value(&default_script_file)
                .help(SCRIPT_FILE_HELP)
                .required(false)
                .num_args(1),
        )
        .arg(
            Arg::new(DATABASE_FILE)
                .short('d')
                .long(DATABASE_FILE)
                .env(DATABASE_FILE)
                .value_name(DATABASE_FILE_VALUE_NAME)
                .default_value(&default_database_file)
                .help(DATABASE_FILE_HELP)
                .required(false)
                .num_args(1),
        )
        .arg(
            Arg::new(EXEC_COMMAND)
                .long(EXEC_COMMAND)
                .value_name(EXEC_COMMAND_VALUE_NAME)
                .help(EXEC_COMMAND_HELP)
                .required(false)
                .num_args(1)
                .action(ArgAction::Append)
                .value_delimiter(' ')
                .value_terminator(";"),
        )
        .arg(Arg::new(FIND).help(FIND_HELP))
        .get_matches();

    let script_file = matches
        .get_one(SCRIPT_FILE)
        .unwrap_or(&default_script_file)
        .to_owned();
    let database_file = matches
        .get_one(DATABASE_FILE)
        .unwrap_or(&default_database_file);
    let projects_dir = matches
        .get_one(PROJECTS_DIR)
        .unwrap_or(&default_projects_dir)
        .to_owned();
    let param = if let Some(p) = matches.get_one::<String>(FIND) {
        p
    } else {
        ""
    };
    let mut input = InputHandler::new(&param);
    let mut output = OutputHandler::new(projects_dir);
    let scriptfile = ScriptFile::new(script_file);
    let database = Database::new(database_file)?;

    println!("");

    if param.len() != 0 {
        let count = output.show_results(database.find(input.line.as_str())?)?;
        if count == 1 {
            if let Some(result) = output.get_selected() {
                if let Some(command) = matches.get_many::<String>(EXEC_COMMAND) {
                    scriptfile
                        .write_cd_and_exec(&result, &command.map(|c| c.to_owned()).collect())?;
                } else {
                    scriptfile.write_cd_only(&result)?;
                }
                return Ok(());
            }
        }
    } else {
        let _ = output.show_results(database.find(".*")?);
    }
    while running.load(Ordering::SeqCst) {
        match input.read_key()? {
            Enter => {
                if let Some(result) = output.get_selected() {
                    database.increment(&result)?;
                    if let Some(command) = matches.get_many::<String>(EXEC_COMMAND) {
                        scriptfile
                            .write_cd_and_exec(&result, &command.map(|c| c.to_owned()).collect())?;
                    } else {
                        scriptfile.write_cd_only(&result)?;
                    }
                }
                break;
            }
            ArrowUp => {
                output.up()?;
            }
            ArrowDown => {
                output.down()?;
            }
            Escape => {
                break;
            }
            _ => {
                output.show_results(database.find(input.line.as_str())?)?;
            }
        }
    }
    output.clear_results()?;
    Ok(())
}
