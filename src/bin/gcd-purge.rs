use anyhow::Result;
use clap::{Command, Arg, crate_version, crate_authors};
use std::path::Path;
use std::convert::TryInto;
use indicatif::ProgressBar;

use gcd_cli::db::Database;
use gcd_cli::config::Config;
use gcd_cli::constants::*;

fn main() -> Result<()> {
    let config = Config::new()?;
    let default_database_file = config.database_file();

    let matches = Command::new("gcd-purge")
        .version(&crate_version!()[..])
        .author(crate_authors!())
        .about("Purge the database, removes all dead projects by checking if the folder exists.")
        .arg(
            Arg::new(DATABASE_FILE)
                .short('d')
                .long(DATABASE_FILE)
                .env(DATABASE_FILE)
                .value_name(DATABASE_FILE_VALUE_NAME)
                .default_value(&default_database_file)
                .help(DATABASE_FILE_HELP)
                .required(false)
                .num_args(1),
        ).get_matches();

    let database_file = matches
        .get_one(DATABASE_FILE)
        .unwrap_or(&default_database_file);

    let database = Database::new(database_file)?;
    let projects = database.all()?;
    let progress_bar = ProgressBar::new(projects.len().try_into()?);
    progress_bar.println("Purging projects database");
    for project in projects {
        if !Path::new(&project).exists() {
            progress_bar.set_message(format!("purge {} from database", project));
            database.remove(&project)?;
        }
        progress_bar.inc(1);
    }
    progress_bar.finish_with_message("done.");
    Ok(())
}
