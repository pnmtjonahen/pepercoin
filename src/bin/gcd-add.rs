use clap::{crate_version, crate_authors, Command, Arg};
use anyhow::Result;


use gcd_cli::config::Config;
use gcd_cli::constants::*;
use gcd_cli::db::Database;
use gcd_cli::lib::current_dir;

fn main() -> Result<()> {
    let config = Config::new()?;
    let default_database_file = config.database_file();

    let matches = Command::new("gcd-add")
        .version(&crate_version!()[..])
        .author(crate_authors!())
        .about("Add current folder to the database.")
        .arg(
            Arg::new(DATABASE_FILE)
                .short('d')
                .long(DATABASE_FILE)
                .env(DATABASE_FILE)
                .value_name(DATABASE_FILE_VALUE_NAME)
                .default_value(&default_database_file)
                .help(DATABASE_FILE_HELP)
                .required(false)
                .num_args(1),
        )
        .get_matches();

    let database_file = matches
        .get_one(DATABASE_FILE)
        .unwrap_or(&default_database_file);

    let database = Database::new(database_file)?;
    database.append(&current_dir()?)?;
    Ok(())
}
