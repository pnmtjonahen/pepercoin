# GCD - A different cd

Primary used for quickly navigating between git projects.

Supports
- git projects, the GCD database is populated using the gcd-init/gcd-update commands.
- non project folders, these can be added to the GCD database with gcd-add command.
- aliases for git project and non git project folders can be set using the gcd-alias command.

Note:
A git project folders is a folder that has a '.git' sub folder, no other checks are done.

## Installation

### prerequisites
A working Rust build system https://www.rust-lang.org/tools/install.

### using cargo
```bash
carco install gcd-cli
```
### from source
Get the code from.

```bash
git clone https://gitlab.com/pnmtjonahen/pepercoin.git
```

Build and install it.
```bash
cargo install --path .
```

### configuration and use

#### binaries
Install the binaries as described above.

#### script (unix)
Either copy the gcd script from the scripts folder into your ${HOME}/bin folder. (or equivalent, like ${HOME}/.cargo/bin)

Or create a gcd.sh script as follows
```bash
#!/bin/bash

SCRIPT_FILE="${HOME}/.gcd/gcd-cd.sh"

gcd-select $@

if [[ -f "${SCRIPT_FILE}" ]]; then
  source ${SCRIPT_FILE}
  rm ${SCRIPT_FILE}
fi
```

#### bash alias
And lastly, make an entry into your .bash_aliases file like:

```
alias gcd='source /home/user/.cargo/bin/gcd.sh'
```

Note: sourcing the shell script is needed else the internal 'cd' command will not work as expected.

#### script (windows)
Either copy the gcd.bat script from the scripts folder into your path folder. like ${APPDATA}/.cargo/bin

Or create a gcd script as follows
```bat
@echo off
call gcd-select
if exist %APPDATA%\.gcd\gcd-cd.bat (
    %APPDATA%\.gcd\gcd-cd.bat
    del /s %APPDATA%\.gcd\gcd-cd.bat
)
```

#### first time use

after installation call gcd-init to initialize the database and perform the initial search for projects
```bash
gcd-init
```

## Basic use
gcd is made up out of a number of different applications. Gcd-select is the main one, it is however not called directly but via a shell script.

- gcd-select, called from the gcd script and is used to select a project.
- gcd-init, initializes the database. it will overwrite the complete database. loosing all usage data, and aliases.
- gcd-alias, set an alias for the current folder, if the current folder is not a known project. nothing will happen.
- gcd-add, adds the current folder to the database.
- gcd-purge, purges aka removes all project from the database that are no longer available.
- gcd-delete, removes the current folder from the database.
- gcd-update, updates the database, as opposed to the gcd-init, this will only add projects that are not yet in the database
- gcd-mv, move a project onto a new folder. When moving git projects on disc use this to update the gcd database with the new location.


## Developing
## Code coverage

### Prerequisits
```sh
sudo snap install jq
rustup component add llvm-tools-preview
cargo install cargo-binutils
cargo install rustfilt
```

and
```
rustup default nightly
```

### generate coverage data
```sh
cargo clean
LLVM_PROFILE_FILE="./target/coverage/gcd-%p-%m.profraw" RUSTFLAGS="-Z instrument-coverage" cargo test
cargo profdata -- merge -sparse ./target/coverage/gcd-*.profraw -o ./target/coverage/gcd.profdata

```

### generate coverage report
```sh
cargo cov -- report \
    $( \
      for file in \
        $( \
          RUSTFLAGS="-Z instrument-coverage" \
            cargo test --tests --no-run --message-format=json \
              | jq -r "select(.profile.test == true) | .filenames[]" \
              | grep -v dSYM - \
        ); \
      do \
        printf "%s %s " -object $file; \
      done \
    ) \
    -object ./target/debug/gcd-add \
    -object ./target/debug/gcd-alias \
    -object ./target/debug/gcd-delete \
    -object ./target/debug/gcd-init \
    -object ./target/debug/gcd-mv \
    -object ./target/debug/gcd-purge \
    -object ./target/debug/gcd-select \
    -object ./target/debug/gcd-update \
  --instr-profile=./target/coverage/gcd.profdata --summary-only --ignore-filename-regex='/.cargo/registry|/rustc'
```

```sh
cargo cov -- show \
    $( \
      for file in \
        $( \
          RUSTFLAGS="-Z instrument-coverage" \
            cargo test --tests --no-run --message-format=json \
              | jq -r "select(.profile.test == true) | .filenames[]" \
              | grep -v dSYM - \
        ); \
      do \
        printf "%s %s " -object $file; \
      done \
    ) \
    -object ./target/debug/gcd-add \
    -object ./target/debug/gcd-alias \
    -object ./target/debug/gcd-delete \
    -object ./target/debug/gcd-init \
    -object ./target/debug/gcd-mv \
    -object ./target/debug/gcd-purge \
    -object ./target/debug/gcd-select \
    -object ./target/debug/gcd-update \
  --instr-profile=./target/coverage/gcd.profdata --format=html --output-dir=./target/coverage/report --ignore-filename-regex='/.cargo/registry|/rustc'
```

### Distribution

## Ubunto
Create a deb file with

```bash
cargo deb
```

This wil create a .deb file for the current os level.

## MacOSX

TBD

## Windows

